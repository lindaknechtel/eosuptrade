plugins {
    id("io.spring.dependency-management") version "1.0.8.RELEASE"
    id("org.springframework.boot") version "2.1.6.RELEASE"
    java
}

group = "eos-uptrade"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation("com.google.guava:guava:28.0-jre")
    implementation("org.springframework.boot:spring-boot-starter")
    testImplementation("junit:junit:4.12")
}

tasks.register<JavaExec>("play") {
    group = "play"
    description = "Plays the game rock, paper, scissors with two players."

    main = "eos.uptrade.controller.Application"
    standardInput = System.`in`
    classpath = sourceSets["main"].runtimeClasspath
}