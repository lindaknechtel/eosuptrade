package eos.uptrade.controller;

import org.junit.Test;

public class RunnerTest {

    /**
     * Provokes an exception with an illegal argument for the strategy.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testRunGamesWithWrongStrategy() {

        final int numberOfGames = 5;
        final int wrongStrategy = 3;
        Runner.runGames(numberOfGames, wrongStrategy);
    }

    /**
     * Provokes an exception with an illegal argument for the number of games variable.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testRunGamesWithWrongNumberOfGames() {

        final int numberOfGames = 0;
        final int validStrategy = 2;
        Runner.runGames(numberOfGames, validStrategy);
    }

    /**
     * Tests that the method does not fail with valid arguments.
     */
    @Test
    public void testRunGamesWithValidStrategyAndValidNumberOfGames() {

        final int numberOfGames = 5;
        final int validStrategy = 2;
        Runner.runGames(numberOfGames, validStrategy);
    }


}