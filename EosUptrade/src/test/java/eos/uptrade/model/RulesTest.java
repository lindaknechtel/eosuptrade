package eos.uptrade.model;

import org.junit.Test;

/**
 * Tests all kind of situations in the game.
 */
public class RulesTest {

    private final Rules rules = new Rules();

    @Test
    public void determineResultRockVsRock() {

        final GameElement computer1 = GameElement.ROCK;
        final GameElement computer2 = GameElement.ROCK;

        assert rules.determineResult(computer1, computer2).equals(Result.TIE);
    }

    @Test
    public void determineResultRockVsPaper() {

        final GameElement computer1 = GameElement.ROCK;
        final GameElement computer2 = GameElement.PAPER;

        assert rules.determineResult(computer1, computer2).equals(Result.COMPUTER_TWO);
    }

    @Test
    public void determineResultRockVsScissors() {

        final GameElement computer1 = GameElement.ROCK;
        final GameElement computer2 = GameElement.SCISSORS;

        assert rules.determineResult(computer1, computer2).equals(Result.COMPUTER_ONE);
    }

    @Test
    public void determineResultPaperVsRock() {

        final GameElement computer1 = GameElement.PAPER;
        final GameElement computer2 = GameElement.ROCK;

        assert rules.determineResult(computer1, computer2).equals(Result.COMPUTER_ONE);
    }

    @Test
    public void determineResultPaperVsPaper() {

        final GameElement computer1 = GameElement.PAPER;
        final GameElement computer2 = GameElement.PAPER;

        assert rules.determineResult(computer1, computer2).equals(Result.TIE);
    }

    @Test
    public void determineResultPaperVsScissors() {

        final GameElement computer1 = GameElement.PAPER;
        final GameElement computer2 = GameElement.SCISSORS;

        assert rules.determineResult(computer1, computer2).equals(Result.COMPUTER_TWO);
    }

    @Test
    public void determineResultScissorsVsRock() {

        final GameElement computer1 = GameElement.SCISSORS;
        final GameElement computer2 = GameElement.ROCK;

        assert rules.determineResult(computer1, computer2).equals(Result.COMPUTER_TWO);
    }

    @Test
    public void determineResultScissorsVsPaper() {

        final GameElement computer1 = GameElement.SCISSORS;
        final GameElement computer2 = GameElement.PAPER;

        assert rules.determineResult(computer1, computer2).equals(Result.COMPUTER_ONE);
    }

    @Test
    public void determineResultScissorsVsScissors() {

        final GameElement computer1 = GameElement.SCISSORS;
        final GameElement computer2 = GameElement.SCISSORS;

        assert rules.determineResult(computer1, computer2).equals(Result.TIE);
    }
}