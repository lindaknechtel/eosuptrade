package eos.uptrade.controller;

import eos.uptrade.model.Result;
import eos.uptrade.model.Round;

import java.util.Map;

class Runner {

    /**
     * Gets inputs for the run and runs the chosen strategy. It returns the results of the games.
     * <p>
     * Strategy handling:
     * <UL>
     *     <LI>1) random</LI>
     *     <LI>2) always rock</LI>
     *     <LI>Everything else is not supported.</LI>
     * </UL>
     */
    static Map<Integer, Round> runGames(final int numberOfGames, final int strategy) {

        if (numberOfGames < 1)
            throw new IllegalArgumentException("The given value for the number of games is not supported");

        if (strategy == 1)
            return Strategy.random(numberOfGames);
        else if (strategy == 2)
            return Strategy.alwaysRock(numberOfGames);
        else
            throw new IllegalArgumentException("The given value for the strategy is not supported");
    }

    /**
     * Determines overall computeOverallWinner of all games.
     */
    static Result computeOverallWinner(final Map<Integer, Round> results) {

        int countComputer1 = 0;
        int countComputer2 = 0;

        for (final Round round : results.values()) {

            if (round.getResult().equals(Result.COMPUTER_ONE))
                countComputer1++;
            else if (round.getResult().equals(Result.COMPUTER_TWO))
                countComputer2++;
        }

        if (countComputer1 > countComputer2)
            return Result.COMPUTER_ONE;
        else if (countComputer1 < countComputer2)
            return Result.COMPUTER_TWO;
        else
            return Result.TIE;
    }
}