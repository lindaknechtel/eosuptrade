package eos.uptrade.controller;

import eos.uptrade.model.GameElement;
import eos.uptrade.model.Result;
import eos.uptrade.model.Rules;
import eos.uptrade.model.Round;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Provides helper methods to define the game strategy.
 */
class Strategy {

    /**
     * The random strategy determines a random game element (rock, paper, scissors) for each of the two players and determines the computeOverallWinner.
     * This procedure is done accordingly to the given number of games.
     */
    static Map<Integer, Round> random(final int numberOfGames) {

        final Map<Integer, Round> results = new HashMap<>();
        final GameElement[] gameElements = GameElement.values();

        for (int i = 0; i < numberOfGames; i++) {

            // Determine random value for computer 1.
            final int randomValue_1 = new Random().nextInt(gameElements.length);
            final GameElement gameElement_1 = gameElements[randomValue_1];

            // Determine random value for computer 2.
            final int randomValue_2 = new Random().nextInt(gameElements.length);
            final GameElement gameElement_2 = gameElements[randomValue_2];

            final Result result = Rules.determineResult(gameElement_1, gameElement_2);

            results.put(i, new Round(gameElement_1, gameElement_2, result));
        }

        return results;
    }


    /**
     * The always rock strategy sets the game element to rock for each of the two players and determines the computeOverallWinner.
     * This procedure is done accordingly to the given number of games.
     */
    static Map<Integer, Round> alwaysRock(final int numberOfGames) {

        final Map<Integer, Round> results = new HashMap<>();

        for (int i = 0; i < numberOfGames; i++) {

            final GameElement gameElement_1 = GameElement.ROCK;
            final GameElement gameElement_2 = GameElement.ROCK;

            final Result result = Rules.determineResult(gameElement_1, gameElement_2);

            results.put(i, new Round(gameElement_1, gameElement_2, result));
        }

        return results;
    }
}