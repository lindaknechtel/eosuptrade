package eos.uptrade.controller;

import eos.uptrade.model.Result;
import eos.uptrade.model.Round;
import eos.uptrade.view.Console;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Map;

@SpringBootApplication
public class Application implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    /**
     * Brings everything together. Retrieves the user inputs from console, runs the games and provides the results.
     */
    @Override
    public void run(String... args) {

        final int numberOfGames = Console.getNumberOfGames();
        final int strategy = Console.getStrategy();

        final Map<Integer, Round> results = Runner.runGames(numberOfGames, strategy);
        final Result winner = Runner.computeOverallWinner(results);

        Console.showResults(results, winner);
    }
}