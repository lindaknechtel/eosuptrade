package eos.uptrade.view;

import eos.uptrade.model.Result;
import eos.uptrade.model.Round;

import java.util.Map;
import java.util.Scanner;

/**
 * Responsible for user interaction on the console.
 */
public class Console {

    /**
     * Retrieves user input for the strategy.
     */
    public static int getStrategy() {
        final Scanner scanner = new Scanner(System.in);

        System.out.println("Select a strategy: 1) for random or 2) for always rock!");
        return scanner.nextInt();
    }

    /**
     * Retrieves user input for the number of games.
     */
    public static int getNumberOfGames() {
        final Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter the number of games!");
        return scanner.nextInt();
    }

    /**
     * Prints the results of the games to the console.
     */
    public static void showResults(final Map<Integer, Round> gameResults, final Result winner) {

        System.out.println("Game results\n");
        System.out.println("Round, Player 1, Player 2, Result");

        for (int i = 0; i < gameResults.size(); i++) {

            final Round round = gameResults.get(i);

            gameResults.get(i);
            System.out.println(String.format("%d, %s, %s, %s", i, round.getComputer1(), round.getComputer2(), round.getResult()));
        }

        if (winner.equals(Result.TIE))
            System.out.println("The overall result is a tie.");
        else
            System.out.println(String.format("The overall winner is: %s", winner));
    }
}