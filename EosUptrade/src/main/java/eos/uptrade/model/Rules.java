package eos.uptrade.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.google.common.collect.MoreCollectors.onlyElement;

/**
 * Set rules: rock beats scissors, scissors beats paper, paper beats rock, both the same results in a tie.
 */
public class Rules {

    private static List<Round> rounds = Arrays.asList(
            new Round(GameElement.ROCK, GameElement.ROCK, Result.TIE),
            new Round(GameElement.ROCK, GameElement.PAPER, Result.COMPUTER_TWO),
            new Round(GameElement.ROCK, GameElement.SCISSORS, Result.COMPUTER_ONE),

            new Round(GameElement.PAPER, GameElement.ROCK, Result.COMPUTER_ONE),
            new Round(GameElement.PAPER, GameElement.PAPER, Result.TIE),
            new Round(GameElement.PAPER, GameElement.SCISSORS, Result.COMPUTER_TWO),

            new Round(GameElement.SCISSORS, GameElement.ROCK, Result.COMPUTER_TWO),
            new Round(GameElement.SCISSORS, GameElement.PAPER, Result.COMPUTER_ONE),
            new Round(GameElement.SCISSORS, GameElement.SCISSORS, Result.TIE)
    );

    /**
     * Filters game rounds list for the right constellation.
     */
    public static Result determineResult(final GameElement one, final GameElement two) {

        return rounds
                .stream()
                .filter(round -> round.getComputer1().equals(one))
                .filter(round -> round.getComputer2().equals(two))
                .collect(onlyElement()).getResult();
    }
}

