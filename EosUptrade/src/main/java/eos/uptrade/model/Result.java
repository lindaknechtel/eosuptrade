package eos.uptrade.model;

/**
 * Sets the return values of the game.
 *
 * <UL>
 *     <LI>COMPUTER_ONE means computer one is the winner.</LI>
 *     <LI>COMPUTER_TWO means computer two is the winner.</LI>
 *     <LI>TIE means nobody wins.</LI>
 * </UL>
 */
public enum Result {
    COMPUTER_ONE,
    COMPUTER_TWO,
    TIE
}