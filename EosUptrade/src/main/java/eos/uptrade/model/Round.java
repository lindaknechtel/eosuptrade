package eos.uptrade.model;

/**
 * Defines a game constellation: game element of computer 1, game element of computer 2 and the result of this round.
 */
public class Round {

    private final GameElement computer1;
    private final GameElement computer2;
    private final Result result;

    public Round(final GameElement computer1, final GameElement computer2, final Result result) {
        this.computer1 = computer1;
        this.computer2 = computer2;
        this.result = result;
    }

    public GameElement getComputer1() {
        return computer1;
    }

    public GameElement getComputer2() {
        return computer2;
    }

    public Result getResult() {
        return result;
    }
}