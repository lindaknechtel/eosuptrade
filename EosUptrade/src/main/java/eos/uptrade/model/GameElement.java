package eos.uptrade.model;

/**
 * Sets the elements of the game: rock, paper, scissors.
 */
public enum GameElement {
    ROCK,
    PAPER,
    SCISSORS
}
