Play paper, rock, scissors.
---------------------------

Just call the following in the EosUptrade folder:

gradlew play

Afterwards the number of games and the game strategy is requested from the user. 

Requirements:
- JDK 11 (f.e. from here: https://openjdk.java.net/install/)